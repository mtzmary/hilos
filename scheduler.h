#ifndef SCHEDULER_H_INCLUDED
#define SCHEDULER_H_INCLUDED
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>


int n;
int i;
int p[20];
pthread_mutex_t llave;
typedef struct nodo{
    int prioridad;
	char nombre[20];
	struct nodo* siguiente;
}nodo;
nodo* primero = NULL;

void insertarNodo();
void *recorrer();

void desplegarPila();

#endif // SCHEDULER_H_INCLUDED
